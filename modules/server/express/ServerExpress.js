const IServer  = require('../../../interfaces/IServer');
const express = require('express');
const IRoute = require('../../../interfaces/IRoute');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require(process.cwd()+'/config/swagger.json');

module.exports = class ServerExpress extends IServer{

    constructor(){
        super();
        this.app = new express(); 
        this.app.use(express.json())
        this.app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    }

    setRoute(routers){
        try{
            if(!(routers instanceof IRoute)){
                throw new Error('Router is not instance IRoute')
            }

            const routeList = routers.getRoute();

            if(routeList.length){

                for(const route of routeList){
                    console.log('\x1b[33m'+route.url+'\x1b[0m', '\x1b[32m'+route.method+'\x1b[0m')
                    this.app[route.method](route.url, route.handler);
                }

            }

        }catch(e){
            throw e
        }
    }

    run(port, handler){
        this.app.listen(port, handler)
    }

}