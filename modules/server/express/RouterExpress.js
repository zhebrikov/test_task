const IRoute = require("../../../interfaces/IRoute");

module.exports = class RouterExpress extends IRoute{
    constructor(){
        super();
    }

     /**
     * 
     * @param {string} url 
     * @param {string} method POST|GET|PUT|PATCH|DELETE 
     * @param {function} handler  
     * @param {Object} fields 
     */
    setRoute(url, method, handler, fields={}){
        this.routers.push({
            url,
            method,
            handler,
            fields
        });

        return this;
    }
}