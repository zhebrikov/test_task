const IDB = require("../../../interfaces/IDB");
const knex = require('knex');
const {
    host,
    user,
    port,
    password,
    name
} = require('../../../config/db');

module.exports = class KnexDb extends IDB{
    constructor(){
        super();

        this.connect = knex({
            client: 'pg',
            connection: {
              host : host,
              port : port,
              user : user,
              password : password,
              database : name
            }
          });
    };

    async find(tableName, select='*', where = {}){
      return await this.connect(tableName).where(where).select(select);
    }

    async findById(tableName, id){
      return await this.connect(tableName).where({
        id
      }).select('*');
    }

    async create(tableName, object){
      return await this.connect(tableName).insert(object).returning('*');
    }

    async delete(tableName, id){
      return await this.connect(tableName).where({id}).delete();
    }

    async update(tableName, id, update={}){
      return await this.connect(tableName).where({id}).update(update, Object.keys(update));
    }

    async migration(){
      await this.connect.migrate.latest()
    }
}