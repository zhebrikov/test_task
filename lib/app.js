const { readdir, stat } = require('node:fs/promises');
const { join } = require('node:path');

module.exports = class App{

    /**
     * 
     * @param {Object} config 
     */
    constructor(config = {}){
        this.server = config.server || 'express';
        this.db = config.db || 'knex';
    }

    async createInstanceServer(){
        try{
            const dirInstances = join(
                process.cwd(), 'modules', 'server'
            )

            const contentDir = await readdir(dirInstances);

            const isExistsDir = contentDir.find( cd => cd === this.server );

            if(!isExistsDir){
                throw new Error('Instance dir is not found')
            }

            const server = this.server.substring(0, 1).toUpperCase() + this.server.substring(1);

            const filePath = join(dirInstances, this.server, 'Server'+server +  '.js');

            const isExistsFile = await stat(filePath);

            if(!isExistsFile){
                throw new Error('File server is not found')
            };

            const serverModule = require(filePath);

            return new serverModule();

        }catch(e){
            throw e;
        }
    }

    async createInstanceRouter(){
        try{
            const dirInstances = join(
                process.cwd(), 'modules', 'server'
            )

            const contentDir = await readdir(dirInstances);

            const isExistsDir = contentDir.find( cd => cd === this.server );

            if(!isExistsDir){
                throw new Error('Instance router dir is not found')
            }

            const server = this.server.substring(0, 1).toUpperCase() + this.server.substring(1);

            const filePath = join(dirInstances, this.server, 'Router'+server +  '.js');

            const isExistsFile = await stat(filePath);

            if(!isExistsFile){
                throw new Error('File router is not found')
            };

            const routerModule = require(filePath);

            return new routerModule();

        }catch(e){
            throw e;
        }
    }

    async createInstanceDb(){
        try{
            const dirInstances = join(
                process.cwd(), 'modules', 'db'
            )

            const contentDir = await readdir(dirInstances);

            const isExistsDir = contentDir.find( cd => cd === this.db );

            if(!isExistsDir){
                throw new Error('Instance router dir is not found')
            }

            const filePath = join(dirInstances, this.db, this.db +  'Db.js');

            const isExistsFile = await stat(filePath);

            if(!isExistsFile){
                throw new Error('File router is not found')
            };

            const routerModule = require(filePath);

            return new routerModule();

        }catch(e){
            throw e;
        }
    }

    async init(){
        try{
            return {
                server: await this.createInstanceServer(),
                router: await this.createInstanceRouter(),
                db: await this.createInstanceDb()
    
            }
        }catch(e){
            throw e;
        }
    }

}