const config = require('../config/routes.json');
const { readdir } = require('node:fs/promises');
const { join } = require('node:path');

module.exports = async (routeInstance, dependencies) => {

    const pathHandlers = join(process.cwd(), 'requestHandlers');

    const listHandlers = await readdir(pathHandlers);

    const instanceHandler = {};

    for(const handler of listHandlers){
        const pathHandler = join(pathHandlers, handler);

        instanceHandler[handler.replace('.js', '')] = require(pathHandler)(dependencies);
    }

    for(const handler of Object.keys(config)){

        for(const method of Object.keys(config[handler])){

            const methodString = method;

            for(const rout of Object.keys(config[handler][method])){

                const { handler: hand } = config[handler][method][rout];

                const url = '/'+[handler, rout].join('/');

                routeInstance.setRoute(
                    url,
                    methodString,
                    instanceHandler[handler][hand]
                )

            }

        }

    }

    return routeInstance;

}