/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
    .createTable('product', function (table) {
        table.increments('id');
        table.string('name', 255).notNullable();
        table.text('description');
        table.string('manufacturer', 255);
        table.integer('category_id', 11);
        table.foreign('category_id').references('category.id');
    });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    knex.schema.dropForeign("category_id");
    return knex.schema
    .dropTable("product")
};
