const ErrorNotFoubd = require("../errors/ErrorNotFound");

module.exports = (dependencies) => {
    const { db } = dependencies;

    return {
        create: async (req, res) => {
            try{
                const { body: { name } } = req;

                const objectNewCategory = {
                    name
                };

                if(!!req.body.hasOwnProperty('parent_id') && req.body.parent_id){
                    const childProduct = await db.find('product', 'id', {
                        category_id: req.body.parent_id
                    });
    
                    if(childProduct.length){
                        throw new Error('The parent category must not contain products.')
                    }

                    objectNewCategory['parent_id'] = req.body.parent_id;
                }

                const newCategory = await db.create('category', objectNewCategory);

                res.send(newCategory)
            }catch(e) {
                console.error(e);
                res.status(500).send({
                    status: 'Error',
                    message:  e.message
                })
            }
        },
        update: async (req, res) => {
            try{
                const { params: { id } } = req;

                const categoryInfo = await db.findById('category', id);

                if(!categoryInfo.length){
                    throw new ErrorNotFoubd('Category is not found')
                }
            }catch(e){
                console.error(e);
                res.status(500).send({
                    status: 'Error',
                    message:  e.message
                })
            }
        },  
        remove: async (req, res) => {
            try{
                const { params: { id } } = req;

                const categoryInfo = await db.findById('category', id);

                if(!categoryInfo.length){
                    throw new ErrorNotFoubd('Category is not found')
                }

                db.delete('category', id)

                res.send(categoryInfo)

            }catch(e){
                console.error(e);
                res.status(500).send({
                    status: 'Error',
                    message:  e.message
                })
            }
        },
        getOne: async (req, res) => {
            try{
                const { params: { id } } = req;

                const categoryInfo = await db.findById('category', id);

                if(!categoryInfo.length){
                    throw new ErrorNotFoubd('Category is not found')
                }

                res.send(categoryInfo)
            }catch(e) {
                console.error(e);
                res.status(500).send({
                    status: 'Error',
                    message:  e.message
                })
            }
        }

    }
}