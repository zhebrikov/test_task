const ErrorNotFound = require("../errors/ErrorNotFound");

module.exports = (dependencies) => {
    const { db } = dependencies;

    return {
        /**
         * 
         * @param {Object} req 
         * @param {Object} res 
         */
        create: async (req, res) => {
            try{
                const { name } = req.body;

                if(!!req.body.category_id){
                    const childCategory = await db.find('category', 'id', {
                        parent_id: req.body.category_id
                    });
    
                    if(childCategory.length){
                        throw new Error('A product cannot be added to a category if it has a child')
                    }
                }

                const createObject = {
                    name,
                }

                if(req.body.category_id){
                    createObject['category_id'] = req.body.category_id;
                }

                if(req.body.manufacturer){
                    createObject['manufacturer'] = req.body.manufacturer;
                }

                if(req.body.description){
                    createObject['description'] = req.body.description;
                }

                const newProduct = await db.create('product',createObject);

                res.send(newProduct)
            }catch(e) {
                console.error(e);
                res.status(e.code || 500).send({
                    status: 'Error',
                    message:  e.message
                })
            }
        },
        update: async (req, res) => {
            try{
                const { params: { id } } = req;

                const productInfo = await db.findById('product', id);

                if(!productInfo.length){
                    throw new ErrorNotFound('Prouduct is not found')
                }

                const result = await db.update('product', id, req.body);

                res.send(result);
            }catch(e){
                console.error(e);
                res.status(500).send({
                    status: 'Error',
                    message:  e.message
                })
            }
        },  
        remove: async (req, res) => {
            try{
                const { params: { id } } = req;

                const productInfo = await db.findById('product', id);

                if(!productInfo.length){
                    throw new ErrorNotFound('Prouduct is not found')
                }

                db.delete('product', id)

                res.send(productInfo)

            }catch(e){
                console.error(e);
                res.status(e.code || 500).send({
                    status: 'Error',
                    message:  e.message
                })
            }
        },
        getOne: async (req, res) => {
            try{
                const { params: { id } } = req;

                const productInfo = await db.findById('product', id);

                if(!productInfo.length){
                    throw new ErrorNotFound('Prouduct is not found')
                }

                res.send(productInfo)
            }catch(e) {
                console.error(e);
                res.status(e.code || 500).send({
                    status: 'Error',
                    message:  e.message
                })
            }
        }

    }
}