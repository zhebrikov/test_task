const App = require('./lib/app');
const initRouter = require('./lib/initRouter');
// run
(async () => {
    const app = new App();
    const { server, router, db } = await app.init();
    const routes = await initRouter(router, {db});
    db.migration();
    server.setRoute(routes);
    server.run(3000);
})();