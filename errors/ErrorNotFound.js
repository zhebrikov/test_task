module.exports = class ErrorNotFoubd extends Error{
    constructor(message){
        super(message);
        this.message = message;
        this.name = 'ErrorNotFoubd';
        this.code = 404;
    }
}